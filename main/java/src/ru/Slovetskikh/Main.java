package ru.Slovetskikh;

import ru.Slovetskikh.agregation.AgregateSourceData;
import ru.Slovetskikh.agregation.AgregateTokenData;
import ru.Slovetskikh.json.GetJson;
import ru.Slovetskikh.json.parse.ParseVideosJson;
import ru.Slovetskikh.model.Video;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();

        GetJson mainJson = new GetJson("https://www.mocky.io/v2/5c51b9dd3400003252129fb5", "главный");
        mainJson.start();
        mainJson.join();
        ParseVideosJson parsedMainJson = new ParseVideosJson(mainJson.getJsonString());
        List<Video> videos = parsedMainJson.getParsedVideos();
        Thread [] threadsJsonSourceData = new Thread[videos.size()];
        Thread [] threadsJsonTokenData = new Thread[videos.size()];
        Thread [] threadsAgregateSourceData = new Thread[videos.size()];
        Thread [] threadsAgregateTokenData = new Thread[videos.size()];

        for (int i = 0; i < videos.size(); i++) {
            Thread jsonSourceData = new GetJson(videos.get(i).sourceDataUrl, "парсим данные источника");
            Thread jsonTokenData = new GetJson(videos.get(i).tokenDataUrl, "парсим данные токена");
            jsonSourceData.start();
            jsonTokenData.start();
            threadsJsonSourceData[i] = jsonSourceData;
            threadsJsonTokenData[i] = jsonTokenData;
        }
        for (int i = 0; i < videos.size(); i++) {
            threadsJsonSourceData[i].join();
            GetJson getJson = (GetJson) threadsJsonSourceData[i];
            Thread agregateSourceData = new AgregateSourceData(getJson.getJsonString(), videos.get(i));
            agregateSourceData.start();
            threadsAgregateSourceData[i] = agregateSourceData;
            threadsJsonTokenData[i].join();
            GetJson getJson1 = (GetJson) threadsJsonTokenData[i];
            Thread agregateTokenData = new AgregateTokenData(getJson1.getJsonString(), videos.get(i));
            agregateTokenData.start();
            threadsAgregateTokenData[i] =  agregateTokenData;
        }

        for (int i = 0; i < videos.size(); i++) {
            threadsAgregateSourceData[i].join();
            threadsAgregateTokenData[i].join();
        }
        System.out.println("Агрегированные данные: ");
        System.out.println(videos);
        long endTime = System.currentTimeMillis();
        System.out.println("That took " + (endTime - startTime)*0.001 + " seconds");
    }
}
