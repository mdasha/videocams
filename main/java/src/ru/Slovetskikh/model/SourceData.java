package ru.Slovetskikh.model;

import java.util.Objects;

public class SourceData {
    public String sourceDataUrl;
    public String urlType;
    public String videoUrl;

    public String getSourceDataUrl() {
        return sourceDataUrl;
    }

    public void setSourceDataUrl(String sourceDataUrl) {
        this.sourceDataUrl = sourceDataUrl;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString() {
        return "SourceData{" +
                "sourceDataUrl='" + sourceDataUrl + '\'' +
                ", urlType='" + urlType + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SourceData)) return false;
        SourceData that = (SourceData) o;
        return Objects.equals(sourceDataUrl, that.sourceDataUrl) && Objects.equals(urlType, that.urlType) && Objects.equals(videoUrl, that.videoUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceDataUrl, urlType, videoUrl);
    }
}
