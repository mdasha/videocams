package ru.Slovetskikh.model;

import java.util.Objects;

public class Video {
    public long id;
    public String sourceDataUrl;
    public String tokenDataUrl;
    public String urlType;
    public String videoUrl;
    public String value;
    public String ttl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSourceDataUrl() {
        return sourceDataUrl;
    }

    public void setSourceDataUrl(String sourceDataUrl) {
        this.sourceDataUrl = sourceDataUrl;
    }

    public String getTokenDataUrl() {
        return tokenDataUrl;
    }

    public void setTokenDataUrl(String tokenDataUrl) {
        this.tokenDataUrl = tokenDataUrl;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Video)) return false;
        Video video = (Video) o;
        return id == video.id && Objects.equals(sourceDataUrl, video.sourceDataUrl) && Objects.equals(tokenDataUrl, video.tokenDataUrl) && Objects.equals(urlType, video.urlType) && Objects.equals(videoUrl, video.videoUrl) && Objects.equals(value, video.value) && Objects.equals(ttl, video.ttl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sourceDataUrl, tokenDataUrl, urlType, videoUrl, value, ttl);
    }

    @Override
    public String toString() {
        return "\n\t{" + "\n\t\t"+
                "id: " + id + ", \n\t\t"+
                "urlType: " + urlType + ",\n\t\t"+
                "videoUrl: " + videoUrl + ",\n\t\t"+
                "value: " + value + ",\n\t\t"+
                "ttl: " + ttl + ",\n\t"+
                '}';
    }
}
