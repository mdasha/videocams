package ru.Slovetskikh.model;
import java.util.Objects;

public class TokenData {
    public String tokenDataUrl;
    public String value;
    public String ttl;

    public String getTokenDataUrl() {
        return tokenDataUrl;
    }

    public void setTokenDataUrl(String tokenDataUrl) {
        this.tokenDataUrl = tokenDataUrl;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TokenData)) return false;
        TokenData tokenData = (TokenData) o;
        return Objects.equals(tokenDataUrl, tokenData.tokenDataUrl) && Objects.equals(value, tokenData.value) && Objects.equals(ttl, tokenData.ttl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenDataUrl, value, ttl);
    }

    @Override
    public String toString() {
        return "TokenData{" +
                "tokenDataUrl='" + tokenDataUrl + '\'' +
                ", value='" + value + '\'' +
                ", ttl='" + ttl + '\'' +
                '}';
    }
}
