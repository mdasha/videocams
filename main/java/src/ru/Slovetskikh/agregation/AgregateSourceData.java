package ru.Slovetskikh.agregation;

import ru.Slovetskikh.json.parse.ParseSourceDataJson;
import ru.Slovetskikh.model.SourceData;
import ru.Slovetskikh.model.Video;

public class AgregateSourceData extends Thread {
    public String jsonString;
    public Video video;
    public static int i;

    public AgregateSourceData(String jsonString, Video video) {
        this.jsonString = jsonString;
        this.video = video;
    }

    @Override
    public void run() {
        try {
            System.out.println("Поток: " + (++i)*10 + ", агрегируем source: " + video.sourceDataUrl);
            this.getAggregatedVideos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAggregatedVideos() {
            ParseSourceDataJson parsedSourceDataJson = new ParseSourceDataJson(jsonString);
            SourceData sourceData = parsedSourceDataJson.getParsedSourceData();
            video.setUrlType(sourceData.urlType);
            video.setVideoUrl(sourceData.videoUrl);
    }
}
