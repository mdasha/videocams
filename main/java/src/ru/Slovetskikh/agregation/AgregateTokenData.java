package ru.Slovetskikh.agregation;

import ru.Slovetskikh.json.parse.ParseTokenDataJson;
import ru.Slovetskikh.model.TokenData;
import ru.Slovetskikh.model.Video;

public class AgregateTokenData extends Thread {
    public Video video;
    public String jsonString;
    public static int i;

    public AgregateTokenData(String jsonString, Video video) {
        this.video = video;
        this.jsonString = jsonString;
    }

    @Override
    public void run() {
        try {
            System.out.println("Поток: " + (++i)*100 + ", агрегируем token: " + video.tokenDataUrl);
            this.getAggregatedVideos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAggregatedVideos()  {
            ParseTokenDataJson parsedTokenDataJson = new ParseTokenDataJson(jsonString);
            TokenData tokenData = parsedTokenDataJson.getParsedTokenData();
            video.setValue(tokenData.value);
            video.setTtl(tokenData.ttl);
    }
}
