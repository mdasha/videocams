package ru.Slovetskikh.json;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class GetJson extends Thread {
    private final String urlString;
    private String jsonString;
    private final String description;
    public static int i;

    public String getJsonString() {
        return jsonString;
    }

    public GetJson(String urlString, String description) {
        this.urlString = urlString;
        this.description = description;
    }

    @Override
    public void run() {
        try {
            System.out.println("Поток: " + ++i + " ("+ description +"), urlString: " + urlString);
            this.jsonString = readUrl(this.urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
