package ru.Slovetskikh.json.parse;

import com.google.gson.Gson;
import ru.Slovetskikh.model.SourceData;

public class ParseSourceDataJson {
    private final String jsonString;

    public ParseSourceDataJson(String jsonString) {
        this.jsonString = jsonString;
    }

    public SourceData getParsedSourceData() {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, SourceData.class);
    }
}
