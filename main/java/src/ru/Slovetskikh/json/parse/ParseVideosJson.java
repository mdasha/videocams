package ru.Slovetskikh.json.parse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ru.Slovetskikh.model.Video;

import java.lang.reflect.Type;
import java.util.List;

public class ParseVideosJson {
    private final String jsonOutput;

    public ParseVideosJson(String jsonOutput) {
        this.jsonOutput = jsonOutput;
    }

    public List<Video> getParsedVideos() {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Video>>(){}.getType();
        return gson.fromJson(this.jsonOutput, listType);
    }
}
