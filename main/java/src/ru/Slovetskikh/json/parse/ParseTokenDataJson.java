package ru.Slovetskikh.json.parse;

import com.google.gson.Gson;
import ru.Slovetskikh.model.TokenData;

public class ParseTokenDataJson {
    private final String jsonString;

    public ParseTokenDataJson(String jsonString) {
        this.jsonString = jsonString;
    }

    public TokenData getParsedTokenData() {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, TokenData.class);
    }
}
